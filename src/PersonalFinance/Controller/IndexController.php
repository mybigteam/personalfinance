<?php
namespace PersonalFinance\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use PersonalFinance\Db\UserQuery;

class IndexController
{
    public function indexAction()
    {
        $users = UserQuery::create()->find();

        return new JsonResponse($users);
    }
}
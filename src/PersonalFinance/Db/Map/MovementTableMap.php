<?php

namespace PersonalFinance\Db\Map;

use PersonalFinance\Db\Movement;
use PersonalFinance\Db\MovementQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'movement' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class MovementTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'PersonalFinance.Db.Map.MovementTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'movement';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PersonalFinance\\Db\\Movement';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PersonalFinance.Db.Movement';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the ID_MOVEMENT field
     */
    const ID_MOVEMENT = 'movement.ID_MOVEMENT';

    /**
     * the column name for the MOVEMENT_TYPE field
     */
    const MOVEMENT_TYPE = 'movement.MOVEMENT_TYPE';

    /**
     * the column name for the ID_NATURE field
     */
    const ID_NATURE = 'movement.ID_NATURE';

    /**
     * the column name for the PARCEL_COUNT field
     */
    const PARCEL_COUNT = 'movement.PARCEL_COUNT';

    /**
     * the column name for the CYCLE_TYPE field
     */
    const CYCLE_TYPE = 'movement.CYCLE_TYPE';

    /**
     * the column name for the CYCLE_INTERVAL field
     */
    const CYCLE_INTERVAL = 'movement.CYCLE_INTERVAL';

    /**
     * the column name for the EXPIRATION_DAY field
     */
    const EXPIRATION_DAY = 'movement.EXPIRATION_DAY';

    /**
     * the column name for the START_DATE field
     */
    const START_DATE = 'movement.START_DATE';

    /**
     * the column name for the AMOUNT field
     */
    const AMOUNT = 'movement.AMOUNT';

    /**
     * the column name for the DESCRIPTION field
     */
    const DESCRIPTION = 'movement.DESCRIPTION';

    /**
     * the column name for the CREATE_DATE field
     */
    const CREATE_DATE = 'movement.CREATE_DATE';

    /**
     * the column name for the UPDATE_DATE field
     */
    const UPDATE_DATE = 'movement.UPDATE_DATE';

    /**
     * the column name for the STATUS field
     */
    const STATUS = 'movement.STATUS';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdMovement', 'MovementType', 'IdNature', 'ParcelCount', 'CycleType', 'CycleInterval', 'ExpirationDay', 'StartDate', 'Amount', 'Description', 'CreateDate', 'UpdateDate', 'Status', ),
        self::TYPE_STUDLYPHPNAME => array('idMovement', 'movementType', 'idNature', 'parcelCount', 'cycleType', 'cycleInterval', 'expirationDay', 'startDate', 'amount', 'description', 'createDate', 'updateDate', 'status', ),
        self::TYPE_COLNAME       => array(MovementTableMap::ID_MOVEMENT, MovementTableMap::MOVEMENT_TYPE, MovementTableMap::ID_NATURE, MovementTableMap::PARCEL_COUNT, MovementTableMap::CYCLE_TYPE, MovementTableMap::CYCLE_INTERVAL, MovementTableMap::EXPIRATION_DAY, MovementTableMap::START_DATE, MovementTableMap::AMOUNT, MovementTableMap::DESCRIPTION, MovementTableMap::CREATE_DATE, MovementTableMap::UPDATE_DATE, MovementTableMap::STATUS, ),
        self::TYPE_RAW_COLNAME   => array('ID_MOVEMENT', 'MOVEMENT_TYPE', 'ID_NATURE', 'PARCEL_COUNT', 'CYCLE_TYPE', 'CYCLE_INTERVAL', 'EXPIRATION_DAY', 'START_DATE', 'AMOUNT', 'DESCRIPTION', 'CREATE_DATE', 'UPDATE_DATE', 'STATUS', ),
        self::TYPE_FIELDNAME     => array('id_movement', 'movement_type', 'id_nature', 'parcel_count', 'cycle_type', 'cycle_interval', 'expiration_day', 'start_date', 'amount', 'description', 'create_date', 'update_date', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdMovement' => 0, 'MovementType' => 1, 'IdNature' => 2, 'ParcelCount' => 3, 'CycleType' => 4, 'CycleInterval' => 5, 'ExpirationDay' => 6, 'StartDate' => 7, 'Amount' => 8, 'Description' => 9, 'CreateDate' => 10, 'UpdateDate' => 11, 'Status' => 12, ),
        self::TYPE_STUDLYPHPNAME => array('idMovement' => 0, 'movementType' => 1, 'idNature' => 2, 'parcelCount' => 3, 'cycleType' => 4, 'cycleInterval' => 5, 'expirationDay' => 6, 'startDate' => 7, 'amount' => 8, 'description' => 9, 'createDate' => 10, 'updateDate' => 11, 'status' => 12, ),
        self::TYPE_COLNAME       => array(MovementTableMap::ID_MOVEMENT => 0, MovementTableMap::MOVEMENT_TYPE => 1, MovementTableMap::ID_NATURE => 2, MovementTableMap::PARCEL_COUNT => 3, MovementTableMap::CYCLE_TYPE => 4, MovementTableMap::CYCLE_INTERVAL => 5, MovementTableMap::EXPIRATION_DAY => 6, MovementTableMap::START_DATE => 7, MovementTableMap::AMOUNT => 8, MovementTableMap::DESCRIPTION => 9, MovementTableMap::CREATE_DATE => 10, MovementTableMap::UPDATE_DATE => 11, MovementTableMap::STATUS => 12, ),
        self::TYPE_RAW_COLNAME   => array('ID_MOVEMENT' => 0, 'MOVEMENT_TYPE' => 1, 'ID_NATURE' => 2, 'PARCEL_COUNT' => 3, 'CYCLE_TYPE' => 4, 'CYCLE_INTERVAL' => 5, 'EXPIRATION_DAY' => 6, 'START_DATE' => 7, 'AMOUNT' => 8, 'DESCRIPTION' => 9, 'CREATE_DATE' => 10, 'UPDATE_DATE' => 11, 'STATUS' => 12, ),
        self::TYPE_FIELDNAME     => array('id_movement' => 0, 'movement_type' => 1, 'id_nature' => 2, 'parcel_count' => 3, 'cycle_type' => 4, 'cycle_interval' => 5, 'expiration_day' => 6, 'start_date' => 7, 'amount' => 8, 'description' => 9, 'create_date' => 10, 'update_date' => 11, 'status' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('movement');
        $this->setPhpName('Movement');
        $this->setClassName('\\PersonalFinance\\Db\\Movement');
        $this->setPackage('PersonalFinance.Db');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ID_MOVEMENT', 'IdMovement', 'INTEGER', true, null, null);
        $this->addColumn('MOVEMENT_TYPE', 'MovementType', 'CHAR', false, null, null);
        $this->addColumn('ID_NATURE', 'IdNature', 'INTEGER', false, null, null);
        $this->addColumn('PARCEL_COUNT', 'ParcelCount', 'INTEGER', false, null, null);
        $this->addColumn('CYCLE_TYPE', 'CycleType', 'CHAR', true, null, null);
        $this->addColumn('CYCLE_INTERVAL', 'CycleInterval', 'INTEGER', false, null, null);
        $this->addColumn('EXPIRATION_DAY', 'ExpirationDay', 'DATE', false, null, null);
        $this->addColumn('START_DATE', 'StartDate', 'DATE', false, null, null);
        $this->addColumn('AMOUNT', 'Amount', 'DECIMAL', false, 12, null);
        $this->addColumn('DESCRIPTION', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('CREATE_DATE', 'CreateDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('UPDATE_DATE', 'UpdateDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('STATUS', 'Status', 'BOOLEAN', false, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMovement', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMovement', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {

            return (int) $row[
                            $indexType == TableMap::TYPE_NUM
                            ? 0 + $offset
                            : self::translateFieldName('IdMovement', TableMap::TYPE_PHPNAME, $indexType)
                        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? MovementTableMap::CLASS_DEFAULT : MovementTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     * @return array (Movement object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = MovementTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MovementTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MovementTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MovementTableMap::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MovementTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MovementTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MovementTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MovementTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MovementTableMap::ID_MOVEMENT);
            $criteria->addSelectColumn(MovementTableMap::MOVEMENT_TYPE);
            $criteria->addSelectColumn(MovementTableMap::ID_NATURE);
            $criteria->addSelectColumn(MovementTableMap::PARCEL_COUNT);
            $criteria->addSelectColumn(MovementTableMap::CYCLE_TYPE);
            $criteria->addSelectColumn(MovementTableMap::CYCLE_INTERVAL);
            $criteria->addSelectColumn(MovementTableMap::EXPIRATION_DAY);
            $criteria->addSelectColumn(MovementTableMap::START_DATE);
            $criteria->addSelectColumn(MovementTableMap::AMOUNT);
            $criteria->addSelectColumn(MovementTableMap::DESCRIPTION);
            $criteria->addSelectColumn(MovementTableMap::CREATE_DATE);
            $criteria->addSelectColumn(MovementTableMap::UPDATE_DATE);
            $criteria->addSelectColumn(MovementTableMap::STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.ID_MOVEMENT');
            $criteria->addSelectColumn($alias . '.MOVEMENT_TYPE');
            $criteria->addSelectColumn($alias . '.ID_NATURE');
            $criteria->addSelectColumn($alias . '.PARCEL_COUNT');
            $criteria->addSelectColumn($alias . '.CYCLE_TYPE');
            $criteria->addSelectColumn($alias . '.CYCLE_INTERVAL');
            $criteria->addSelectColumn($alias . '.EXPIRATION_DAY');
            $criteria->addSelectColumn($alias . '.START_DATE');
            $criteria->addSelectColumn($alias . '.AMOUNT');
            $criteria->addSelectColumn($alias . '.DESCRIPTION');
            $criteria->addSelectColumn($alias . '.CREATE_DATE');
            $criteria->addSelectColumn($alias . '.UPDATE_DATE');
            $criteria->addSelectColumn($alias . '.STATUS');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(MovementTableMap::DATABASE_NAME)->getTable(MovementTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getServiceContainer()->getDatabaseMap(MovementTableMap::DATABASE_NAME);
      if (!$dbMap->hasTable(MovementTableMap::TABLE_NAME)) {
        $dbMap->addTableObject(new MovementTableMap());
      }
    }

    /**
     * Performs a DELETE on the database, given a Movement or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Movement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovementTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PersonalFinance\Db\Movement) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MovementTableMap::DATABASE_NAME);
            $criteria->add(MovementTableMap::ID_MOVEMENT, (array) $values, Criteria::IN);
        }

        $query = MovementQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) { MovementTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) { MovementTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the movement table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return MovementQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Movement or Criteria object.
     *
     * @param mixed               $criteria Criteria or Movement object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovementTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Movement object
        }


        // Set the correct dbName
        $query = MovementQuery::create()->mergeWith($criteria);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = $query->doInsert($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

} // MovementTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
MovementTableMap::buildTableMap();

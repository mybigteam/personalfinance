<?php

namespace PersonalFinance\Db\Map;

use PersonalFinance\Db\Parcel;
use PersonalFinance\Db\ParcelQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'parcel' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ParcelTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'PersonalFinance.Db.Map.ParcelTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'parcel';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PersonalFinance\\Db\\Parcel';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PersonalFinance.Db.Parcel';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the ID_PARCEL field
     */
    const ID_PARCEL = 'parcel.ID_PARCEL';

    /**
     * the column name for the ID_MOVEMENT field
     */
    const ID_MOVEMENT = 'parcel.ID_MOVEMENT';

    /**
     * the column name for the DESCRIPTION field
     */
    const DESCRIPTION = 'parcel.DESCRIPTION';

    /**
     * the column name for the PARCEL_NUMBER field
     */
    const PARCEL_NUMBER = 'parcel.PARCEL_NUMBER';

    /**
     * the column name for the AMOUNT field
     */
    const AMOUNT = 'parcel.AMOUNT';

    /**
     * the column name for the DATE field
     */
    const DATE = 'parcel.DATE';

    /**
     * the column name for the PAYMENT_DATE field
     */
    const PAYMENT_DATE = 'parcel.PAYMENT_DATE';

    /**
     * the column name for the EXPIRATION_DATE field
     */
    const EXPIRATION_DATE = 'parcel.EXPIRATION_DATE';

    /**
     * the column name for the CREATE_DATE field
     */
    const CREATE_DATE = 'parcel.CREATE_DATE';

    /**
     * the column name for the UPDATE_DATE field
     */
    const UPDATE_DATE = 'parcel.UPDATE_DATE';

    /**
     * the column name for the STATUS field
     */
    const STATUS = 'parcel.STATUS';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdParcel', 'IdMovement', 'Description', 'ParcelNumber', 'Amount', 'Date', 'PaymentDate', 'ExpirationDate', 'CreateDate', 'UpdateDate', 'Status', ),
        self::TYPE_STUDLYPHPNAME => array('idParcel', 'idMovement', 'description', 'parcelNumber', 'amount', 'date', 'paymentDate', 'expirationDate', 'createDate', 'updateDate', 'status', ),
        self::TYPE_COLNAME       => array(ParcelTableMap::ID_PARCEL, ParcelTableMap::ID_MOVEMENT, ParcelTableMap::DESCRIPTION, ParcelTableMap::PARCEL_NUMBER, ParcelTableMap::AMOUNT, ParcelTableMap::DATE, ParcelTableMap::PAYMENT_DATE, ParcelTableMap::EXPIRATION_DATE, ParcelTableMap::CREATE_DATE, ParcelTableMap::UPDATE_DATE, ParcelTableMap::STATUS, ),
        self::TYPE_RAW_COLNAME   => array('ID_PARCEL', 'ID_MOVEMENT', 'DESCRIPTION', 'PARCEL_NUMBER', 'AMOUNT', 'DATE', 'PAYMENT_DATE', 'EXPIRATION_DATE', 'CREATE_DATE', 'UPDATE_DATE', 'STATUS', ),
        self::TYPE_FIELDNAME     => array('id_parcel', 'id_movement', 'description', 'parcel_number', 'amount', 'date', 'payment_date', 'expiration_date', 'create_date', 'update_date', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdParcel' => 0, 'IdMovement' => 1, 'Description' => 2, 'ParcelNumber' => 3, 'Amount' => 4, 'Date' => 5, 'PaymentDate' => 6, 'ExpirationDate' => 7, 'CreateDate' => 8, 'UpdateDate' => 9, 'Status' => 10, ),
        self::TYPE_STUDLYPHPNAME => array('idParcel' => 0, 'idMovement' => 1, 'description' => 2, 'parcelNumber' => 3, 'amount' => 4, 'date' => 5, 'paymentDate' => 6, 'expirationDate' => 7, 'createDate' => 8, 'updateDate' => 9, 'status' => 10, ),
        self::TYPE_COLNAME       => array(ParcelTableMap::ID_PARCEL => 0, ParcelTableMap::ID_MOVEMENT => 1, ParcelTableMap::DESCRIPTION => 2, ParcelTableMap::PARCEL_NUMBER => 3, ParcelTableMap::AMOUNT => 4, ParcelTableMap::DATE => 5, ParcelTableMap::PAYMENT_DATE => 6, ParcelTableMap::EXPIRATION_DATE => 7, ParcelTableMap::CREATE_DATE => 8, ParcelTableMap::UPDATE_DATE => 9, ParcelTableMap::STATUS => 10, ),
        self::TYPE_RAW_COLNAME   => array('ID_PARCEL' => 0, 'ID_MOVEMENT' => 1, 'DESCRIPTION' => 2, 'PARCEL_NUMBER' => 3, 'AMOUNT' => 4, 'DATE' => 5, 'PAYMENT_DATE' => 6, 'EXPIRATION_DATE' => 7, 'CREATE_DATE' => 8, 'UPDATE_DATE' => 9, 'STATUS' => 10, ),
        self::TYPE_FIELDNAME     => array('id_parcel' => 0, 'id_movement' => 1, 'description' => 2, 'parcel_number' => 3, 'amount' => 4, 'date' => 5, 'payment_date' => 6, 'expiration_date' => 7, 'create_date' => 8, 'update_date' => 9, 'status' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('parcel');
        $this->setPhpName('Parcel');
        $this->setClassName('\\PersonalFinance\\Db\\Parcel');
        $this->setPackage('PersonalFinance.Db');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ID_PARCEL', 'IdParcel', 'INTEGER', true, null, null);
        $this->addColumn('ID_MOVEMENT', 'IdMovement', 'INTEGER', false, null, null);
        $this->addColumn('DESCRIPTION', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('PARCEL_NUMBER', 'ParcelNumber', 'INTEGER', false, 4, null);
        $this->addColumn('AMOUNT', 'Amount', 'DECIMAL', false, 12, null);
        $this->addColumn('DATE', 'Date', 'DATE', false, null, null);
        $this->addColumn('PAYMENT_DATE', 'PaymentDate', 'DATE', false, null, null);
        $this->addColumn('EXPIRATION_DATE', 'ExpirationDate', 'DATE', false, null, null);
        $this->addColumn('CREATE_DATE', 'CreateDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('UPDATE_DATE', 'UpdateDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('STATUS', 'Status', 'BOOLEAN', false, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdParcel', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdParcel', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {

            return (int) $row[
                            $indexType == TableMap::TYPE_NUM
                            ? 0 + $offset
                            : self::translateFieldName('IdParcel', TableMap::TYPE_PHPNAME, $indexType)
                        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ParcelTableMap::CLASS_DEFAULT : ParcelTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     * @return array (Parcel object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ParcelTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ParcelTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ParcelTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ParcelTableMap::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ParcelTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ParcelTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ParcelTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ParcelTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ParcelTableMap::ID_PARCEL);
            $criteria->addSelectColumn(ParcelTableMap::ID_MOVEMENT);
            $criteria->addSelectColumn(ParcelTableMap::DESCRIPTION);
            $criteria->addSelectColumn(ParcelTableMap::PARCEL_NUMBER);
            $criteria->addSelectColumn(ParcelTableMap::AMOUNT);
            $criteria->addSelectColumn(ParcelTableMap::DATE);
            $criteria->addSelectColumn(ParcelTableMap::PAYMENT_DATE);
            $criteria->addSelectColumn(ParcelTableMap::EXPIRATION_DATE);
            $criteria->addSelectColumn(ParcelTableMap::CREATE_DATE);
            $criteria->addSelectColumn(ParcelTableMap::UPDATE_DATE);
            $criteria->addSelectColumn(ParcelTableMap::STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.ID_PARCEL');
            $criteria->addSelectColumn($alias . '.ID_MOVEMENT');
            $criteria->addSelectColumn($alias . '.DESCRIPTION');
            $criteria->addSelectColumn($alias . '.PARCEL_NUMBER');
            $criteria->addSelectColumn($alias . '.AMOUNT');
            $criteria->addSelectColumn($alias . '.DATE');
            $criteria->addSelectColumn($alias . '.PAYMENT_DATE');
            $criteria->addSelectColumn($alias . '.EXPIRATION_DATE');
            $criteria->addSelectColumn($alias . '.CREATE_DATE');
            $criteria->addSelectColumn($alias . '.UPDATE_DATE');
            $criteria->addSelectColumn($alias . '.STATUS');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ParcelTableMap::DATABASE_NAME)->getTable(ParcelTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getServiceContainer()->getDatabaseMap(ParcelTableMap::DATABASE_NAME);
      if (!$dbMap->hasTable(ParcelTableMap::TABLE_NAME)) {
        $dbMap->addTableObject(new ParcelTableMap());
      }
    }

    /**
     * Performs a DELETE on the database, given a Parcel or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Parcel object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ParcelTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PersonalFinance\Db\Parcel) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ParcelTableMap::DATABASE_NAME);
            $criteria->add(ParcelTableMap::ID_PARCEL, (array) $values, Criteria::IN);
        }

        $query = ParcelQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) { ParcelTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) { ParcelTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the parcel table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ParcelQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Parcel or Criteria object.
     *
     * @param mixed               $criteria Criteria or Parcel object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ParcelTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Parcel object
        }


        // Set the correct dbName
        $query = ParcelQuery::create()->mergeWith($criteria);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = $query->doInsert($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

} // ParcelTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ParcelTableMap::buildTableMap();

<?php

namespace PersonalFinance\Db\Map;

use PersonalFinance\Db\Email;
use PersonalFinance\Db\EmailQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'email' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class EmailTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'PersonalFinance.Db.Map.EmailTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'email';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PersonalFinance\\Db\\Email';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PersonalFinance.Db.Email';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the ID_EMAIL field
     */
    const ID_EMAIL = 'email.ID_EMAIL';

    /**
     * the column name for the ID_USER field
     */
    const ID_USER = 'email.ID_USER';

    /**
     * the column name for the EMAIL field
     */
    const EMAIL = 'email.EMAIL';

    /**
     * the column name for the CREATE_DATE field
     */
    const CREATE_DATE = 'email.CREATE_DATE';

    /**
     * the column name for the UPDATE_DATE field
     */
    const UPDATE_DATE = 'email.UPDATE_DATE';

    /**
     * the column name for the STATUS field
     */
    const STATUS = 'email.STATUS';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdEmail', 'IdUser', 'Email', 'CreateDate', 'UpdateDate', 'Status', ),
        self::TYPE_STUDLYPHPNAME => array('idEmail', 'idUser', 'email', 'createDate', 'updateDate', 'status', ),
        self::TYPE_COLNAME       => array(EmailTableMap::ID_EMAIL, EmailTableMap::ID_USER, EmailTableMap::EMAIL, EmailTableMap::CREATE_DATE, EmailTableMap::UPDATE_DATE, EmailTableMap::STATUS, ),
        self::TYPE_RAW_COLNAME   => array('ID_EMAIL', 'ID_USER', 'EMAIL', 'CREATE_DATE', 'UPDATE_DATE', 'STATUS', ),
        self::TYPE_FIELDNAME     => array('id_email', 'id_user', 'email', 'create_date', 'update_date', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdEmail' => 0, 'IdUser' => 1, 'Email' => 2, 'CreateDate' => 3, 'UpdateDate' => 4, 'Status' => 5, ),
        self::TYPE_STUDLYPHPNAME => array('idEmail' => 0, 'idUser' => 1, 'email' => 2, 'createDate' => 3, 'updateDate' => 4, 'status' => 5, ),
        self::TYPE_COLNAME       => array(EmailTableMap::ID_EMAIL => 0, EmailTableMap::ID_USER => 1, EmailTableMap::EMAIL => 2, EmailTableMap::CREATE_DATE => 3, EmailTableMap::UPDATE_DATE => 4, EmailTableMap::STATUS => 5, ),
        self::TYPE_RAW_COLNAME   => array('ID_EMAIL' => 0, 'ID_USER' => 1, 'EMAIL' => 2, 'CREATE_DATE' => 3, 'UPDATE_DATE' => 4, 'STATUS' => 5, ),
        self::TYPE_FIELDNAME     => array('id_email' => 0, 'id_user' => 1, 'email' => 2, 'create_date' => 3, 'update_date' => 4, 'status' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('email');
        $this->setPhpName('Email');
        $this->setClassName('\\PersonalFinance\\Db\\Email');
        $this->setPackage('PersonalFinance.Db');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ID_EMAIL', 'IdEmail', 'INTEGER', true, null, null);
        $this->addColumn('ID_USER', 'IdUser', 'INTEGER', false, null, null);
        $this->addColumn('EMAIL', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('CREATE_DATE', 'CreateDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('UPDATE_DATE', 'UpdateDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('STATUS', 'Status', 'BOOLEAN', false, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEmail', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEmail', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {

            return (int) $row[
                            $indexType == TableMap::TYPE_NUM
                            ? 0 + $offset
                            : self::translateFieldName('IdEmail', TableMap::TYPE_PHPNAME, $indexType)
                        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EmailTableMap::CLASS_DEFAULT : EmailTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     * @return array (Email object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EmailTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EmailTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EmailTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EmailTableMap::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EmailTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EmailTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EmailTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EmailTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EmailTableMap::ID_EMAIL);
            $criteria->addSelectColumn(EmailTableMap::ID_USER);
            $criteria->addSelectColumn(EmailTableMap::EMAIL);
            $criteria->addSelectColumn(EmailTableMap::CREATE_DATE);
            $criteria->addSelectColumn(EmailTableMap::UPDATE_DATE);
            $criteria->addSelectColumn(EmailTableMap::STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.ID_EMAIL');
            $criteria->addSelectColumn($alias . '.ID_USER');
            $criteria->addSelectColumn($alias . '.EMAIL');
            $criteria->addSelectColumn($alias . '.CREATE_DATE');
            $criteria->addSelectColumn($alias . '.UPDATE_DATE');
            $criteria->addSelectColumn($alias . '.STATUS');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EmailTableMap::DATABASE_NAME)->getTable(EmailTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getServiceContainer()->getDatabaseMap(EmailTableMap::DATABASE_NAME);
      if (!$dbMap->hasTable(EmailTableMap::TABLE_NAME)) {
        $dbMap->addTableObject(new EmailTableMap());
      }
    }

    /**
     * Performs a DELETE on the database, given a Email or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Email object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EmailTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PersonalFinance\Db\Email) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EmailTableMap::DATABASE_NAME);
            $criteria->add(EmailTableMap::ID_EMAIL, (array) $values, Criteria::IN);
        }

        $query = EmailQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) { EmailTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) { EmailTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the email table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EmailQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Email or Criteria object.
     *
     * @param mixed               $criteria Criteria or Email object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EmailTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Email object
        }


        // Set the correct dbName
        $query = EmailQuery::create()->mergeWith($criteria);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = $query->doInsert($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

} // EmailTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EmailTableMap::buildTableMap();

<?php

namespace PersonalFinance\Db\Base;

use \Exception;
use \PDO;
use PersonalFinance\Db\Movement as ChildMovement;
use PersonalFinance\Db\MovementQuery as ChildMovementQuery;
use PersonalFinance\Db\Map\MovementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'movement' table.
 *
 *
 *
 * @method     ChildMovementQuery orderByIdMovement($order = Criteria::ASC) Order by the id_movement column
 * @method     ChildMovementQuery orderByMovementType($order = Criteria::ASC) Order by the movement_type column
 * @method     ChildMovementQuery orderByIdNature($order = Criteria::ASC) Order by the id_nature column
 * @method     ChildMovementQuery orderByParcelCount($order = Criteria::ASC) Order by the parcel_count column
 * @method     ChildMovementQuery orderByCycleType($order = Criteria::ASC) Order by the cycle_type column
 * @method     ChildMovementQuery orderByCycleInterval($order = Criteria::ASC) Order by the cycle_interval column
 * @method     ChildMovementQuery orderByExpirationDay($order = Criteria::ASC) Order by the expiration_day column
 * @method     ChildMovementQuery orderByStartDate($order = Criteria::ASC) Order by the start_date column
 * @method     ChildMovementQuery orderByAmount($order = Criteria::ASC) Order by the amount column
 * @method     ChildMovementQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildMovementQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method     ChildMovementQuery orderByUpdateDate($order = Criteria::ASC) Order by the update_date column
 * @method     ChildMovementQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildMovementQuery groupByIdMovement() Group by the id_movement column
 * @method     ChildMovementQuery groupByMovementType() Group by the movement_type column
 * @method     ChildMovementQuery groupByIdNature() Group by the id_nature column
 * @method     ChildMovementQuery groupByParcelCount() Group by the parcel_count column
 * @method     ChildMovementQuery groupByCycleType() Group by the cycle_type column
 * @method     ChildMovementQuery groupByCycleInterval() Group by the cycle_interval column
 * @method     ChildMovementQuery groupByExpirationDay() Group by the expiration_day column
 * @method     ChildMovementQuery groupByStartDate() Group by the start_date column
 * @method     ChildMovementQuery groupByAmount() Group by the amount column
 * @method     ChildMovementQuery groupByDescription() Group by the description column
 * @method     ChildMovementQuery groupByCreateDate() Group by the create_date column
 * @method     ChildMovementQuery groupByUpdateDate() Group by the update_date column
 * @method     ChildMovementQuery groupByStatus() Group by the status column
 *
 * @method     ChildMovementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMovementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMovementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMovement findOne(ConnectionInterface $con = null) Return the first ChildMovement matching the query
 * @method     ChildMovement findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMovement matching the query, or a new ChildMovement object populated from the query conditions when no match is found
 *
 * @method     ChildMovement findOneByIdMovement(int $id_movement) Return the first ChildMovement filtered by the id_movement column
 * @method     ChildMovement findOneByMovementType(string $movement_type) Return the first ChildMovement filtered by the movement_type column
 * @method     ChildMovement findOneByIdNature(int $id_nature) Return the first ChildMovement filtered by the id_nature column
 * @method     ChildMovement findOneByParcelCount(int $parcel_count) Return the first ChildMovement filtered by the parcel_count column
 * @method     ChildMovement findOneByCycleType(string $cycle_type) Return the first ChildMovement filtered by the cycle_type column
 * @method     ChildMovement findOneByCycleInterval(int $cycle_interval) Return the first ChildMovement filtered by the cycle_interval column
 * @method     ChildMovement findOneByExpirationDay(string $expiration_day) Return the first ChildMovement filtered by the expiration_day column
 * @method     ChildMovement findOneByStartDate(string $start_date) Return the first ChildMovement filtered by the start_date column
 * @method     ChildMovement findOneByAmount(string $amount) Return the first ChildMovement filtered by the amount column
 * @method     ChildMovement findOneByDescription(string $description) Return the first ChildMovement filtered by the description column
 * @method     ChildMovement findOneByCreateDate(string $create_date) Return the first ChildMovement filtered by the create_date column
 * @method     ChildMovement findOneByUpdateDate(string $update_date) Return the first ChildMovement filtered by the update_date column
 * @method     ChildMovement findOneByStatus(boolean $status) Return the first ChildMovement filtered by the status column
 *
 * @method     array findByIdMovement(int $id_movement) Return ChildMovement objects filtered by the id_movement column
 * @method     array findByMovementType(string $movement_type) Return ChildMovement objects filtered by the movement_type column
 * @method     array findByIdNature(int $id_nature) Return ChildMovement objects filtered by the id_nature column
 * @method     array findByParcelCount(int $parcel_count) Return ChildMovement objects filtered by the parcel_count column
 * @method     array findByCycleType(string $cycle_type) Return ChildMovement objects filtered by the cycle_type column
 * @method     array findByCycleInterval(int $cycle_interval) Return ChildMovement objects filtered by the cycle_interval column
 * @method     array findByExpirationDay(string $expiration_day) Return ChildMovement objects filtered by the expiration_day column
 * @method     array findByStartDate(string $start_date) Return ChildMovement objects filtered by the start_date column
 * @method     array findByAmount(string $amount) Return ChildMovement objects filtered by the amount column
 * @method     array findByDescription(string $description) Return ChildMovement objects filtered by the description column
 * @method     array findByCreateDate(string $create_date) Return ChildMovement objects filtered by the create_date column
 * @method     array findByUpdateDate(string $update_date) Return ChildMovement objects filtered by the update_date column
 * @method     array findByStatus(boolean $status) Return ChildMovement objects filtered by the status column
 *
 */
abstract class MovementQuery extends ModelCriteria
{

    /**
     * Initializes internal state of \PersonalFinance\Db\Base\MovementQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PersonalFinance\\Db\\Movement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMovementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMovementQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof \PersonalFinance\Db\MovementQuery) {
            return $criteria;
        }
        $query = new \PersonalFinance\Db\MovementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMovement|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MovementTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MovementTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return   ChildMovement A model object, or null if the key is not found
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT ID_MOVEMENT, MOVEMENT_TYPE, ID_NATURE, PARCEL_COUNT, CYCLE_TYPE, CYCLE_INTERVAL, EXPIRATION_DAY, START_DATE, AMOUNT, DESCRIPTION, CREATE_DATE, UPDATE_DATE, STATUS FROM movement WHERE ID_MOVEMENT = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            $obj = new ChildMovement();
            $obj->hydrate($row);
            MovementTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMovement|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MovementTableMap::ID_MOVEMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MovementTableMap::ID_MOVEMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_movement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMovement(1234); // WHERE id_movement = 1234
     * $query->filterByIdMovement(array(12, 34)); // WHERE id_movement IN (12, 34)
     * $query->filterByIdMovement(array('min' => 12)); // WHERE id_movement > 12
     * </code>
     *
     * @param     mixed $idMovement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByIdMovement($idMovement = null, $comparison = null)
    {
        if (is_array($idMovement)) {
            $useMinMax = false;
            if (isset($idMovement['min'])) {
                $this->addUsingAlias(MovementTableMap::ID_MOVEMENT, $idMovement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMovement['max'])) {
                $this->addUsingAlias(MovementTableMap::ID_MOVEMENT, $idMovement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::ID_MOVEMENT, $idMovement, $comparison);
    }

    /**
     * Filter the query on the movement_type column
     *
     * Example usage:
     * <code>
     * $query->filterByMovementType('fooValue');   // WHERE movement_type = 'fooValue'
     * $query->filterByMovementType('%fooValue%'); // WHERE movement_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $movementType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByMovementType($movementType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($movementType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $movementType)) {
                $movementType = str_replace('*', '%', $movementType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovementTableMap::MOVEMENT_TYPE, $movementType, $comparison);
    }

    /**
     * Filter the query on the id_nature column
     *
     * Example usage:
     * <code>
     * $query->filterByIdNature(1234); // WHERE id_nature = 1234
     * $query->filterByIdNature(array(12, 34)); // WHERE id_nature IN (12, 34)
     * $query->filterByIdNature(array('min' => 12)); // WHERE id_nature > 12
     * </code>
     *
     * @param     mixed $idNature The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByIdNature($idNature = null, $comparison = null)
    {
        if (is_array($idNature)) {
            $useMinMax = false;
            if (isset($idNature['min'])) {
                $this->addUsingAlias(MovementTableMap::ID_NATURE, $idNature['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idNature['max'])) {
                $this->addUsingAlias(MovementTableMap::ID_NATURE, $idNature['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::ID_NATURE, $idNature, $comparison);
    }

    /**
     * Filter the query on the parcel_count column
     *
     * Example usage:
     * <code>
     * $query->filterByParcelCount(1234); // WHERE parcel_count = 1234
     * $query->filterByParcelCount(array(12, 34)); // WHERE parcel_count IN (12, 34)
     * $query->filterByParcelCount(array('min' => 12)); // WHERE parcel_count > 12
     * </code>
     *
     * @param     mixed $parcelCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByParcelCount($parcelCount = null, $comparison = null)
    {
        if (is_array($parcelCount)) {
            $useMinMax = false;
            if (isset($parcelCount['min'])) {
                $this->addUsingAlias(MovementTableMap::PARCEL_COUNT, $parcelCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parcelCount['max'])) {
                $this->addUsingAlias(MovementTableMap::PARCEL_COUNT, $parcelCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::PARCEL_COUNT, $parcelCount, $comparison);
    }

    /**
     * Filter the query on the cycle_type column
     *
     * Example usage:
     * <code>
     * $query->filterByCycleType('fooValue');   // WHERE cycle_type = 'fooValue'
     * $query->filterByCycleType('%fooValue%'); // WHERE cycle_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cycleType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByCycleType($cycleType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cycleType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cycleType)) {
                $cycleType = str_replace('*', '%', $cycleType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovementTableMap::CYCLE_TYPE, $cycleType, $comparison);
    }

    /**
     * Filter the query on the cycle_interval column
     *
     * Example usage:
     * <code>
     * $query->filterByCycleInterval(1234); // WHERE cycle_interval = 1234
     * $query->filterByCycleInterval(array(12, 34)); // WHERE cycle_interval IN (12, 34)
     * $query->filterByCycleInterval(array('min' => 12)); // WHERE cycle_interval > 12
     * </code>
     *
     * @param     mixed $cycleInterval The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByCycleInterval($cycleInterval = null, $comparison = null)
    {
        if (is_array($cycleInterval)) {
            $useMinMax = false;
            if (isset($cycleInterval['min'])) {
                $this->addUsingAlias(MovementTableMap::CYCLE_INTERVAL, $cycleInterval['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cycleInterval['max'])) {
                $this->addUsingAlias(MovementTableMap::CYCLE_INTERVAL, $cycleInterval['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::CYCLE_INTERVAL, $cycleInterval, $comparison);
    }

    /**
     * Filter the query on the expiration_day column
     *
     * Example usage:
     * <code>
     * $query->filterByExpirationDay('2011-03-14'); // WHERE expiration_day = '2011-03-14'
     * $query->filterByExpirationDay('now'); // WHERE expiration_day = '2011-03-14'
     * $query->filterByExpirationDay(array('max' => 'yesterday')); // WHERE expiration_day > '2011-03-13'
     * </code>
     *
     * @param     mixed $expirationDay The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByExpirationDay($expirationDay = null, $comparison = null)
    {
        if (is_array($expirationDay)) {
            $useMinMax = false;
            if (isset($expirationDay['min'])) {
                $this->addUsingAlias(MovementTableMap::EXPIRATION_DAY, $expirationDay['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expirationDay['max'])) {
                $this->addUsingAlias(MovementTableMap::EXPIRATION_DAY, $expirationDay['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::EXPIRATION_DAY, $expirationDay, $comparison);
    }

    /**
     * Filter the query on the start_date column
     *
     * Example usage:
     * <code>
     * $query->filterByStartDate('2011-03-14'); // WHERE start_date = '2011-03-14'
     * $query->filterByStartDate('now'); // WHERE start_date = '2011-03-14'
     * $query->filterByStartDate(array('max' => 'yesterday')); // WHERE start_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $startDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByStartDate($startDate = null, $comparison = null)
    {
        if (is_array($startDate)) {
            $useMinMax = false;
            if (isset($startDate['min'])) {
                $this->addUsingAlias(MovementTableMap::START_DATE, $startDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startDate['max'])) {
                $this->addUsingAlias(MovementTableMap::START_DATE, $startDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::START_DATE, $startDate, $comparison);
    }

    /**
     * Filter the query on the amount column
     *
     * Example usage:
     * <code>
     * $query->filterByAmount(1234); // WHERE amount = 1234
     * $query->filterByAmount(array(12, 34)); // WHERE amount IN (12, 34)
     * $query->filterByAmount(array('min' => 12)); // WHERE amount > 12
     * </code>
     *
     * @param     mixed $amount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByAmount($amount = null, $comparison = null)
    {
        if (is_array($amount)) {
            $useMinMax = false;
            if (isset($amount['min'])) {
                $this->addUsingAlias(MovementTableMap::AMOUNT, $amount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amount['max'])) {
                $this->addUsingAlias(MovementTableMap::AMOUNT, $amount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::AMOUNT, $amount, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovementTableMap::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(MovementTableMap::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(MovementTableMap::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the update_date column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdateDate('2011-03-14'); // WHERE update_date = '2011-03-14'
     * $query->filterByUpdateDate('now'); // WHERE update_date = '2011-03-14'
     * $query->filterByUpdateDate(array('max' => 'yesterday')); // WHERE update_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $updateDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByUpdateDate($updateDate = null, $comparison = null)
    {
        if (is_array($updateDate)) {
            $useMinMax = false;
            if (isset($updateDate['min'])) {
                $this->addUsingAlias(MovementTableMap::UPDATE_DATE, $updateDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updateDate['max'])) {
                $this->addUsingAlias(MovementTableMap::UPDATE_DATE, $updateDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovementTableMap::UPDATE_DATE, $updateDate, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(true); // WHERE status = true
     * $query->filterByStatus('yes'); // WHERE status = true
     * </code>
     *
     * @param     boolean|string $status The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_string($status)) {
            $status = in_array(strtolower($status), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(MovementTableMap::STATUS, $status, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMovement $movement Object to remove from the list of results
     *
     * @return ChildMovementQuery The current query, for fluid interface
     */
    public function prune($movement = null)
    {
        if ($movement) {
            $this->addUsingAlias(MovementTableMap::ID_MOVEMENT, $movement->getIdMovement(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the movement table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovementTableMap::DATABASE_NAME);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MovementTableMap::clearInstancePool();
            MovementTableMap::clearRelatedInstancePool();

            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $affectedRows;
    }

    /**
     * Performs a DELETE on the database, given a ChildMovement or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChildMovement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
     public function delete(ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovementTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MovementTableMap::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();


        MovementTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MovementTableMap::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

} // MovementQuery

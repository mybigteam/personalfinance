<?php

namespace PersonalFinance\Db\Base;

use \Exception;
use \PDO;
use PersonalFinance\Db\Parcel as ChildParcel;
use PersonalFinance\Db\ParcelQuery as ChildParcelQuery;
use PersonalFinance\Db\Map\ParcelTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'parcel' table.
 *
 *
 *
 * @method     ChildParcelQuery orderByIdParcel($order = Criteria::ASC) Order by the id_parcel column
 * @method     ChildParcelQuery orderByIdMovement($order = Criteria::ASC) Order by the id_movement column
 * @method     ChildParcelQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildParcelQuery orderByParcelNumber($order = Criteria::ASC) Order by the parcel_number column
 * @method     ChildParcelQuery orderByAmount($order = Criteria::ASC) Order by the amount column
 * @method     ChildParcelQuery orderByDate($order = Criteria::ASC) Order by the date column
 * @method     ChildParcelQuery orderByPaymentDate($order = Criteria::ASC) Order by the payment_date column
 * @method     ChildParcelQuery orderByExpirationDate($order = Criteria::ASC) Order by the expiration_date column
 * @method     ChildParcelQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method     ChildParcelQuery orderByUpdateDate($order = Criteria::ASC) Order by the update_date column
 * @method     ChildParcelQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildParcelQuery groupByIdParcel() Group by the id_parcel column
 * @method     ChildParcelQuery groupByIdMovement() Group by the id_movement column
 * @method     ChildParcelQuery groupByDescription() Group by the description column
 * @method     ChildParcelQuery groupByParcelNumber() Group by the parcel_number column
 * @method     ChildParcelQuery groupByAmount() Group by the amount column
 * @method     ChildParcelQuery groupByDate() Group by the date column
 * @method     ChildParcelQuery groupByPaymentDate() Group by the payment_date column
 * @method     ChildParcelQuery groupByExpirationDate() Group by the expiration_date column
 * @method     ChildParcelQuery groupByCreateDate() Group by the create_date column
 * @method     ChildParcelQuery groupByUpdateDate() Group by the update_date column
 * @method     ChildParcelQuery groupByStatus() Group by the status column
 *
 * @method     ChildParcelQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildParcelQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildParcelQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildParcel findOne(ConnectionInterface $con = null) Return the first ChildParcel matching the query
 * @method     ChildParcel findOneOrCreate(ConnectionInterface $con = null) Return the first ChildParcel matching the query, or a new ChildParcel object populated from the query conditions when no match is found
 *
 * @method     ChildParcel findOneByIdParcel(int $id_parcel) Return the first ChildParcel filtered by the id_parcel column
 * @method     ChildParcel findOneByIdMovement(int $id_movement) Return the first ChildParcel filtered by the id_movement column
 * @method     ChildParcel findOneByDescription(string $description) Return the first ChildParcel filtered by the description column
 * @method     ChildParcel findOneByParcelNumber(int $parcel_number) Return the first ChildParcel filtered by the parcel_number column
 * @method     ChildParcel findOneByAmount(string $amount) Return the first ChildParcel filtered by the amount column
 * @method     ChildParcel findOneByDate(string $date) Return the first ChildParcel filtered by the date column
 * @method     ChildParcel findOneByPaymentDate(string $payment_date) Return the first ChildParcel filtered by the payment_date column
 * @method     ChildParcel findOneByExpirationDate(string $expiration_date) Return the first ChildParcel filtered by the expiration_date column
 * @method     ChildParcel findOneByCreateDate(string $create_date) Return the first ChildParcel filtered by the create_date column
 * @method     ChildParcel findOneByUpdateDate(string $update_date) Return the first ChildParcel filtered by the update_date column
 * @method     ChildParcel findOneByStatus(boolean $status) Return the first ChildParcel filtered by the status column
 *
 * @method     array findByIdParcel(int $id_parcel) Return ChildParcel objects filtered by the id_parcel column
 * @method     array findByIdMovement(int $id_movement) Return ChildParcel objects filtered by the id_movement column
 * @method     array findByDescription(string $description) Return ChildParcel objects filtered by the description column
 * @method     array findByParcelNumber(int $parcel_number) Return ChildParcel objects filtered by the parcel_number column
 * @method     array findByAmount(string $amount) Return ChildParcel objects filtered by the amount column
 * @method     array findByDate(string $date) Return ChildParcel objects filtered by the date column
 * @method     array findByPaymentDate(string $payment_date) Return ChildParcel objects filtered by the payment_date column
 * @method     array findByExpirationDate(string $expiration_date) Return ChildParcel objects filtered by the expiration_date column
 * @method     array findByCreateDate(string $create_date) Return ChildParcel objects filtered by the create_date column
 * @method     array findByUpdateDate(string $update_date) Return ChildParcel objects filtered by the update_date column
 * @method     array findByStatus(boolean $status) Return ChildParcel objects filtered by the status column
 *
 */
abstract class ParcelQuery extends ModelCriteria
{

    /**
     * Initializes internal state of \PersonalFinance\Db\Base\ParcelQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PersonalFinance\\Db\\Parcel', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildParcelQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildParcelQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof \PersonalFinance\Db\ParcelQuery) {
            return $criteria;
        }
        $query = new \PersonalFinance\Db\ParcelQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildParcel|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ParcelTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ParcelTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return   ChildParcel A model object, or null if the key is not found
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT ID_PARCEL, ID_MOVEMENT, DESCRIPTION, PARCEL_NUMBER, AMOUNT, DATE, PAYMENT_DATE, EXPIRATION_DATE, CREATE_DATE, UPDATE_DATE, STATUS FROM parcel WHERE ID_PARCEL = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            $obj = new ChildParcel();
            $obj->hydrate($row);
            ParcelTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildParcel|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ParcelTableMap::ID_PARCEL, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ParcelTableMap::ID_PARCEL, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_parcel column
     *
     * Example usage:
     * <code>
     * $query->filterByIdParcel(1234); // WHERE id_parcel = 1234
     * $query->filterByIdParcel(array(12, 34)); // WHERE id_parcel IN (12, 34)
     * $query->filterByIdParcel(array('min' => 12)); // WHERE id_parcel > 12
     * </code>
     *
     * @param     mixed $idParcel The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByIdParcel($idParcel = null, $comparison = null)
    {
        if (is_array($idParcel)) {
            $useMinMax = false;
            if (isset($idParcel['min'])) {
                $this->addUsingAlias(ParcelTableMap::ID_PARCEL, $idParcel['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idParcel['max'])) {
                $this->addUsingAlias(ParcelTableMap::ID_PARCEL, $idParcel['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::ID_PARCEL, $idParcel, $comparison);
    }

    /**
     * Filter the query on the id_movement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMovement(1234); // WHERE id_movement = 1234
     * $query->filterByIdMovement(array(12, 34)); // WHERE id_movement IN (12, 34)
     * $query->filterByIdMovement(array('min' => 12)); // WHERE id_movement > 12
     * </code>
     *
     * @param     mixed $idMovement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByIdMovement($idMovement = null, $comparison = null)
    {
        if (is_array($idMovement)) {
            $useMinMax = false;
            if (isset($idMovement['min'])) {
                $this->addUsingAlias(ParcelTableMap::ID_MOVEMENT, $idMovement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMovement['max'])) {
                $this->addUsingAlias(ParcelTableMap::ID_MOVEMENT, $idMovement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::ID_MOVEMENT, $idMovement, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the parcel_number column
     *
     * Example usage:
     * <code>
     * $query->filterByParcelNumber(1234); // WHERE parcel_number = 1234
     * $query->filterByParcelNumber(array(12, 34)); // WHERE parcel_number IN (12, 34)
     * $query->filterByParcelNumber(array('min' => 12)); // WHERE parcel_number > 12
     * </code>
     *
     * @param     mixed $parcelNumber The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByParcelNumber($parcelNumber = null, $comparison = null)
    {
        if (is_array($parcelNumber)) {
            $useMinMax = false;
            if (isset($parcelNumber['min'])) {
                $this->addUsingAlias(ParcelTableMap::PARCEL_NUMBER, $parcelNumber['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parcelNumber['max'])) {
                $this->addUsingAlias(ParcelTableMap::PARCEL_NUMBER, $parcelNumber['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::PARCEL_NUMBER, $parcelNumber, $comparison);
    }

    /**
     * Filter the query on the amount column
     *
     * Example usage:
     * <code>
     * $query->filterByAmount(1234); // WHERE amount = 1234
     * $query->filterByAmount(array(12, 34)); // WHERE amount IN (12, 34)
     * $query->filterByAmount(array('min' => 12)); // WHERE amount > 12
     * </code>
     *
     * @param     mixed $amount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByAmount($amount = null, $comparison = null)
    {
        if (is_array($amount)) {
            $useMinMax = false;
            if (isset($amount['min'])) {
                $this->addUsingAlias(ParcelTableMap::AMOUNT, $amount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amount['max'])) {
                $this->addUsingAlias(ParcelTableMap::AMOUNT, $amount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::AMOUNT, $amount, $comparison);
    }

    /**
     * Filter the query on the date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE date = '2011-03-14'
     * $query->filterByDate('now'); // WHERE date = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE date > '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(ParcelTableMap::DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(ParcelTableMap::DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::DATE, $date, $comparison);
    }

    /**
     * Filter the query on the payment_date column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentDate('2011-03-14'); // WHERE payment_date = '2011-03-14'
     * $query->filterByPaymentDate('now'); // WHERE payment_date = '2011-03-14'
     * $query->filterByPaymentDate(array('max' => 'yesterday')); // WHERE payment_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $paymentDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByPaymentDate($paymentDate = null, $comparison = null)
    {
        if (is_array($paymentDate)) {
            $useMinMax = false;
            if (isset($paymentDate['min'])) {
                $this->addUsingAlias(ParcelTableMap::PAYMENT_DATE, $paymentDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymentDate['max'])) {
                $this->addUsingAlias(ParcelTableMap::PAYMENT_DATE, $paymentDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::PAYMENT_DATE, $paymentDate, $comparison);
    }

    /**
     * Filter the query on the expiration_date column
     *
     * Example usage:
     * <code>
     * $query->filterByExpirationDate('2011-03-14'); // WHERE expiration_date = '2011-03-14'
     * $query->filterByExpirationDate('now'); // WHERE expiration_date = '2011-03-14'
     * $query->filterByExpirationDate(array('max' => 'yesterday')); // WHERE expiration_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $expirationDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByExpirationDate($expirationDate = null, $comparison = null)
    {
        if (is_array($expirationDate)) {
            $useMinMax = false;
            if (isset($expirationDate['min'])) {
                $this->addUsingAlias(ParcelTableMap::EXPIRATION_DATE, $expirationDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expirationDate['max'])) {
                $this->addUsingAlias(ParcelTableMap::EXPIRATION_DATE, $expirationDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::EXPIRATION_DATE, $expirationDate, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(ParcelTableMap::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(ParcelTableMap::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the update_date column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdateDate('2011-03-14'); // WHERE update_date = '2011-03-14'
     * $query->filterByUpdateDate('now'); // WHERE update_date = '2011-03-14'
     * $query->filterByUpdateDate(array('max' => 'yesterday')); // WHERE update_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $updateDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByUpdateDate($updateDate = null, $comparison = null)
    {
        if (is_array($updateDate)) {
            $useMinMax = false;
            if (isset($updateDate['min'])) {
                $this->addUsingAlias(ParcelTableMap::UPDATE_DATE, $updateDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updateDate['max'])) {
                $this->addUsingAlias(ParcelTableMap::UPDATE_DATE, $updateDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ParcelTableMap::UPDATE_DATE, $updateDate, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(true); // WHERE status = true
     * $query->filterByStatus('yes'); // WHERE status = true
     * </code>
     *
     * @param     boolean|string $status The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_string($status)) {
            $status = in_array(strtolower($status), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ParcelTableMap::STATUS, $status, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildParcel $parcel Object to remove from the list of results
     *
     * @return ChildParcelQuery The current query, for fluid interface
     */
    public function prune($parcel = null)
    {
        if ($parcel) {
            $this->addUsingAlias(ParcelTableMap::ID_PARCEL, $parcel->getIdParcel(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the parcel table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ParcelTableMap::DATABASE_NAME);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ParcelTableMap::clearInstancePool();
            ParcelTableMap::clearRelatedInstancePool();

            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $affectedRows;
    }

    /**
     * Performs a DELETE on the database, given a ChildParcel or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChildParcel object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
     public function delete(ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ParcelTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ParcelTableMap::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();


        ParcelTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ParcelTableMap::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

} // ParcelQuery

<?php

namespace PersonalFinance\Db\Base;

use \Exception;
use \PDO;
use PersonalFinance\Db\Nature as ChildNature;
use PersonalFinance\Db\NatureQuery as ChildNatureQuery;
use PersonalFinance\Db\Map\NatureTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'nature' table.
 *
 *
 *
 * @method     ChildNatureQuery orderByIdNature($order = Criteria::ASC) Order by the id_nature column
 * @method     ChildNatureQuery orderByNatureType($order = Criteria::ASC) Order by the nature_type column
 * @method     ChildNatureQuery orderByIdUser($order = Criteria::ASC) Order by the id_user column
 * @method     ChildNatureQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildNatureQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method     ChildNatureQuery orderByUpdateDate($order = Criteria::ASC) Order by the update_date column
 * @method     ChildNatureQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildNatureQuery groupByIdNature() Group by the id_nature column
 * @method     ChildNatureQuery groupByNatureType() Group by the nature_type column
 * @method     ChildNatureQuery groupByIdUser() Group by the id_user column
 * @method     ChildNatureQuery groupByDescription() Group by the description column
 * @method     ChildNatureQuery groupByCreateDate() Group by the create_date column
 * @method     ChildNatureQuery groupByUpdateDate() Group by the update_date column
 * @method     ChildNatureQuery groupByStatus() Group by the status column
 *
 * @method     ChildNatureQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildNatureQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildNatureQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildNature findOne(ConnectionInterface $con = null) Return the first ChildNature matching the query
 * @method     ChildNature findOneOrCreate(ConnectionInterface $con = null) Return the first ChildNature matching the query, or a new ChildNature object populated from the query conditions when no match is found
 *
 * @method     ChildNature findOneByIdNature(int $id_nature) Return the first ChildNature filtered by the id_nature column
 * @method     ChildNature findOneByNatureType(string $nature_type) Return the first ChildNature filtered by the nature_type column
 * @method     ChildNature findOneByIdUser(int $id_user) Return the first ChildNature filtered by the id_user column
 * @method     ChildNature findOneByDescription(string $description) Return the first ChildNature filtered by the description column
 * @method     ChildNature findOneByCreateDate(string $create_date) Return the first ChildNature filtered by the create_date column
 * @method     ChildNature findOneByUpdateDate(string $update_date) Return the first ChildNature filtered by the update_date column
 * @method     ChildNature findOneByStatus(boolean $status) Return the first ChildNature filtered by the status column
 *
 * @method     array findByIdNature(int $id_nature) Return ChildNature objects filtered by the id_nature column
 * @method     array findByNatureType(string $nature_type) Return ChildNature objects filtered by the nature_type column
 * @method     array findByIdUser(int $id_user) Return ChildNature objects filtered by the id_user column
 * @method     array findByDescription(string $description) Return ChildNature objects filtered by the description column
 * @method     array findByCreateDate(string $create_date) Return ChildNature objects filtered by the create_date column
 * @method     array findByUpdateDate(string $update_date) Return ChildNature objects filtered by the update_date column
 * @method     array findByStatus(boolean $status) Return ChildNature objects filtered by the status column
 *
 */
abstract class NatureQuery extends ModelCriteria
{

    /**
     * Initializes internal state of \PersonalFinance\Db\Base\NatureQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PersonalFinance\\Db\\Nature', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildNatureQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildNatureQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof \PersonalFinance\Db\NatureQuery) {
            return $criteria;
        }
        $query = new \PersonalFinance\Db\NatureQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildNature|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = NatureTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(NatureTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return   ChildNature A model object, or null if the key is not found
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT ID_NATURE, NATURE_TYPE, ID_USER, DESCRIPTION, CREATE_DATE, UPDATE_DATE, STATUS FROM nature WHERE ID_NATURE = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            $obj = new ChildNature();
            $obj->hydrate($row);
            NatureTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildNature|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(NatureTableMap::ID_NATURE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(NatureTableMap::ID_NATURE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_nature column
     *
     * Example usage:
     * <code>
     * $query->filterByIdNature(1234); // WHERE id_nature = 1234
     * $query->filterByIdNature(array(12, 34)); // WHERE id_nature IN (12, 34)
     * $query->filterByIdNature(array('min' => 12)); // WHERE id_nature > 12
     * </code>
     *
     * @param     mixed $idNature The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByIdNature($idNature = null, $comparison = null)
    {
        if (is_array($idNature)) {
            $useMinMax = false;
            if (isset($idNature['min'])) {
                $this->addUsingAlias(NatureTableMap::ID_NATURE, $idNature['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idNature['max'])) {
                $this->addUsingAlias(NatureTableMap::ID_NATURE, $idNature['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NatureTableMap::ID_NATURE, $idNature, $comparison);
    }

    /**
     * Filter the query on the nature_type column
     *
     * Example usage:
     * <code>
     * $query->filterByNatureType('fooValue');   // WHERE nature_type = 'fooValue'
     * $query->filterByNatureType('%fooValue%'); // WHERE nature_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $natureType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByNatureType($natureType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($natureType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $natureType)) {
                $natureType = str_replace('*', '%', $natureType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(NatureTableMap::NATURE_TYPE, $natureType, $comparison);
    }

    /**
     * Filter the query on the id_user column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUser(1234); // WHERE id_user = 1234
     * $query->filterByIdUser(array(12, 34)); // WHERE id_user IN (12, 34)
     * $query->filterByIdUser(array('min' => 12)); // WHERE id_user > 12
     * </code>
     *
     * @param     mixed $idUser The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByIdUser($idUser = null, $comparison = null)
    {
        if (is_array($idUser)) {
            $useMinMax = false;
            if (isset($idUser['min'])) {
                $this->addUsingAlias(NatureTableMap::ID_USER, $idUser['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUser['max'])) {
                $this->addUsingAlias(NatureTableMap::ID_USER, $idUser['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NatureTableMap::ID_USER, $idUser, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(NatureTableMap::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(NatureTableMap::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(NatureTableMap::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NatureTableMap::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the update_date column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdateDate('2011-03-14'); // WHERE update_date = '2011-03-14'
     * $query->filterByUpdateDate('now'); // WHERE update_date = '2011-03-14'
     * $query->filterByUpdateDate(array('max' => 'yesterday')); // WHERE update_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $updateDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByUpdateDate($updateDate = null, $comparison = null)
    {
        if (is_array($updateDate)) {
            $useMinMax = false;
            if (isset($updateDate['min'])) {
                $this->addUsingAlias(NatureTableMap::UPDATE_DATE, $updateDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updateDate['max'])) {
                $this->addUsingAlias(NatureTableMap::UPDATE_DATE, $updateDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NatureTableMap::UPDATE_DATE, $updateDate, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(true); // WHERE status = true
     * $query->filterByStatus('yes'); // WHERE status = true
     * </code>
     *
     * @param     boolean|string $status The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_string($status)) {
            $status = in_array(strtolower($status), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(NatureTableMap::STATUS, $status, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildNature $nature Object to remove from the list of results
     *
     * @return ChildNatureQuery The current query, for fluid interface
     */
    public function prune($nature = null)
    {
        if ($nature) {
            $this->addUsingAlias(NatureTableMap::ID_NATURE, $nature->getIdNature(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the nature table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NatureTableMap::DATABASE_NAME);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            NatureTableMap::clearInstancePool();
            NatureTableMap::clearRelatedInstancePool();

            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $affectedRows;
    }

    /**
     * Performs a DELETE on the database, given a ChildNature or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChildNature object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
     public function delete(ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NatureTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(NatureTableMap::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();


        NatureTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            NatureTableMap::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

} // NatureQuery

<?php

namespace PersonalFinance\Db\Base;

use \DateTime;
use \Exception;
use \PDO;
use PersonalFinance\Db\MovementQuery as ChildMovementQuery;
use PersonalFinance\Db\Map\MovementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

abstract class Movement implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\PersonalFinance\\Db\\Map\\MovementTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_movement field.
     * @var        int
     */
    protected $id_movement;

    /**
     * The value for the movement_type field.
     * @var        string
     */
    protected $movement_type;

    /**
     * The value for the id_nature field.
     * @var        int
     */
    protected $id_nature;

    /**
     * The value for the parcel_count field.
     * @var        int
     */
    protected $parcel_count;

    /**
     * The value for the cycle_type field.
     * @var        string
     */
    protected $cycle_type;

    /**
     * The value for the cycle_interval field.
     * @var        int
     */
    protected $cycle_interval;

    /**
     * The value for the expiration_day field.
     * @var        string
     */
    protected $expiration_day;

    /**
     * The value for the start_date field.
     * @var        string
     */
    protected $start_date;

    /**
     * The value for the amount field.
     * @var        string
     */
    protected $amount;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the update_date field.
     * @var        string
     */
    protected $update_date;

    /**
     * The value for the status field.
     * @var        boolean
     */
    protected $status;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of PersonalFinance\Db\Base\Movement object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !empty($this->modifiedColumns);
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return in_array($col, $this->modifiedColumns);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return array_unique($this->modifiedColumns);
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (Boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (Boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            while (false !== ($offset = array_search($col, $this->modifiedColumns))) {
                array_splice($this->modifiedColumns, $offset, 1);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Movement</code> instance.  If
     * <code>obj</code> is an instance of <code>Movement</code>, delegates to
     * <code>equals(Movement)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        $thisclazz = get_class($this);
        if (!is_object($obj) || !($obj instanceof $thisclazz)) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey()
            || null === $obj->getPrimaryKey())  {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        if (null !== $this->getPrimaryKey()) {
            return crc32(serialize($this->getPrimaryKey()));
        }

        return crc32(serialize(clone $this));
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return Movement The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     *
     * @return Movement The current object, for fluid interface
     */
    public function importFrom($parser, $data)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), TableMap::TYPE_PHPNAME);

        return $this;
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        return array_keys(get_object_vars($this));
    }

    /**
     * Get the [id_movement] column value.
     *
     * @return   int
     */
    public function getIdMovement()
    {

        return $this->id_movement;
    }

    /**
     * Get the [movement_type] column value.
     *
     * @return   string
     */
    public function getMovementType()
    {

        return $this->movement_type;
    }

    /**
     * Get the [id_nature] column value.
     *
     * @return   int
     */
    public function getIdNature()
    {

        return $this->id_nature;
    }

    /**
     * Get the [parcel_count] column value.
     *
     * @return   int
     */
    public function getParcelCount()
    {

        return $this->parcel_count;
    }

    /**
     * Get the [cycle_type] column value.
     *
     * @return   string
     */
    public function getCycleType()
    {

        return $this->cycle_type;
    }

    /**
     * Get the [cycle_interval] column value.
     *
     * @return   int
     */
    public function getCycleInterval()
    {

        return $this->cycle_interval;
    }

    /**
     * Get the [optionally formatted] temporal [expiration_day] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw \DateTime object will be returned.
     *
     * @return mixed Formatted date/time value as string or \DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpirationDay($format = NULL)
    {
        if ($format === null) {
            return $this->expiration_day;
        } else {
            return $this->expiration_day instanceof \DateTime ? $this->expiration_day->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [start_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw \DateTime object will be returned.
     *
     * @return mixed Formatted date/time value as string or \DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartDate($format = NULL)
    {
        if ($format === null) {
            return $this->start_date;
        } else {
            return $this->start_date instanceof \DateTime ? $this->start_date->format($format) : null;
        }
    }

    /**
     * Get the [amount] column value.
     *
     * @return   string
     */
    public function getAmount()
    {

        return $this->amount;
    }

    /**
     * Get the [description] column value.
     *
     * @return   string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw \DateTime object will be returned.
     *
     * @return mixed Formatted date/time value as string or \DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = NULL)
    {
        if ($format === null) {
            return $this->create_date;
        } else {
            return $this->create_date instanceof \DateTime ? $this->create_date->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [update_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw \DateTime object will be returned.
     *
     * @return mixed Formatted date/time value as string or \DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdateDate($format = NULL)
    {
        if ($format === null) {
            return $this->update_date;
        } else {
            return $this->update_date instanceof \DateTime ? $this->update_date->format($format) : null;
        }
    }

    /**
     * Get the [status] column value.
     *
     * @return   boolean
     */
    public function getStatus()
    {

        return $this->status;
    }

    /**
     * Set the value of [id_movement] column.
     *
     * @param      int $v new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setIdMovement($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_movement !== $v) {
            $this->id_movement = $v;
            $this->modifiedColumns[] = MovementTableMap::ID_MOVEMENT;
        }


        return $this;
    } // setIdMovement()

    /**
     * Set the value of [movement_type] column.
     *
     * @param      string $v new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setMovementType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->movement_type !== $v) {
            $this->movement_type = $v;
            $this->modifiedColumns[] = MovementTableMap::MOVEMENT_TYPE;
        }


        return $this;
    } // setMovementType()

    /**
     * Set the value of [id_nature] column.
     *
     * @param      int $v new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setIdNature($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_nature !== $v) {
            $this->id_nature = $v;
            $this->modifiedColumns[] = MovementTableMap::ID_NATURE;
        }


        return $this;
    } // setIdNature()

    /**
     * Set the value of [parcel_count] column.
     *
     * @param      int $v new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setParcelCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->parcel_count !== $v) {
            $this->parcel_count = $v;
            $this->modifiedColumns[] = MovementTableMap::PARCEL_COUNT;
        }


        return $this;
    } // setParcelCount()

    /**
     * Set the value of [cycle_type] column.
     *
     * @param      string $v new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setCycleType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cycle_type !== $v) {
            $this->cycle_type = $v;
            $this->modifiedColumns[] = MovementTableMap::CYCLE_TYPE;
        }


        return $this;
    } // setCycleType()

    /**
     * Set the value of [cycle_interval] column.
     *
     * @param      int $v new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setCycleInterval($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cycle_interval !== $v) {
            $this->cycle_interval = $v;
            $this->modifiedColumns[] = MovementTableMap::CYCLE_INTERVAL;
        }


        return $this;
    } // setCycleInterval()

    /**
     * Sets the value of [expiration_day] column to a normalized version of the date/time value specified.
     *
     * @param      mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setExpirationDay($v)
    {
        $dt = PropelDateTime::newInstance($v, null, '\DateTime');
        if ($this->expiration_day !== null || $dt !== null) {
            if ($dt !== $this->expiration_day) {
                $this->expiration_day = $dt;
                $this->modifiedColumns[] = MovementTableMap::EXPIRATION_DAY;
            }
        } // if either are not null


        return $this;
    } // setExpirationDay()

    /**
     * Sets the value of [start_date] column to a normalized version of the date/time value specified.
     *
     * @param      mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setStartDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, '\DateTime');
        if ($this->start_date !== null || $dt !== null) {
            if ($dt !== $this->start_date) {
                $this->start_date = $dt;
                $this->modifiedColumns[] = MovementTableMap::START_DATE;
            }
        } // if either are not null


        return $this;
    } // setStartDate()

    /**
     * Set the value of [amount] column.
     *
     * @param      string $v new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setAmount($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->amount !== $v) {
            $this->amount = $v;
            $this->modifiedColumns[] = MovementTableMap::AMOUNT;
        }


        return $this;
    } // setAmount()

    /**
     * Set the value of [description] column.
     *
     * @param      string $v new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = MovementTableMap::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     *
     * @param      mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, '\DateTime');
        if ($this->create_date !== null || $dt !== null) {
            if ($dt !== $this->create_date) {
                $this->create_date = $dt;
                $this->modifiedColumns[] = MovementTableMap::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [update_date] column to a normalized version of the date/time value specified.
     *
     * @param      mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setUpdateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, '\DateTime');
        if ($this->update_date !== null || $dt !== null) {
            if ($dt !== $this->update_date) {
                $this->update_date = $dt;
                $this->modifiedColumns[] = MovementTableMap::UPDATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setUpdateDate()

    /**
     * Sets the value of the [status] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param      boolean|integer|string $v The new value
     * @return   \PersonalFinance\Db\Movement The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = MovementTableMap::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {


            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MovementTableMap::translateFieldName('IdMovement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_movement = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MovementTableMap::translateFieldName('MovementType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->movement_type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : MovementTableMap::translateFieldName('IdNature', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_nature = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : MovementTableMap::translateFieldName('ParcelCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->parcel_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : MovementTableMap::translateFieldName('CycleType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cycle_type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : MovementTableMap::translateFieldName('CycleInterval', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cycle_interval = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : MovementTableMap::translateFieldName('ExpirationDay', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->expiration_day = (null !== $col) ? PropelDateTime::newInstance($col, null, '\DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : MovementTableMap::translateFieldName('StartDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->start_date = (null !== $col) ? PropelDateTime::newInstance($col, null, '\DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : MovementTableMap::translateFieldName('Amount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->amount = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : MovementTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : MovementTableMap::translateFieldName('CreateDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->create_date = (null !== $col) ? PropelDateTime::newInstance($col, null, '\DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : MovementTableMap::translateFieldName('UpdateDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->update_date = (null !== $col) ? PropelDateTime::newInstance($col, null, '\DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : MovementTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = MovementTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating \PersonalFinance\Db\Movement object", 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MovementTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMovementQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Movement::setDeleted()
     * @see Movement::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovementTableMap::DATABASE_NAME);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ChildMovementQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovementTableMap::DATABASE_NAME);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MovementTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MovementTableMap::ID_MOVEMENT)) {
            $modifiedColumns[':p' . $index++]  = 'ID_MOVEMENT';
        }
        if ($this->isColumnModified(MovementTableMap::MOVEMENT_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'MOVEMENT_TYPE';
        }
        if ($this->isColumnModified(MovementTableMap::ID_NATURE)) {
            $modifiedColumns[':p' . $index++]  = 'ID_NATURE';
        }
        if ($this->isColumnModified(MovementTableMap::PARCEL_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'PARCEL_COUNT';
        }
        if ($this->isColumnModified(MovementTableMap::CYCLE_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'CYCLE_TYPE';
        }
        if ($this->isColumnModified(MovementTableMap::CYCLE_INTERVAL)) {
            $modifiedColumns[':p' . $index++]  = 'CYCLE_INTERVAL';
        }
        if ($this->isColumnModified(MovementTableMap::EXPIRATION_DAY)) {
            $modifiedColumns[':p' . $index++]  = 'EXPIRATION_DAY';
        }
        if ($this->isColumnModified(MovementTableMap::START_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'START_DATE';
        }
        if ($this->isColumnModified(MovementTableMap::AMOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'AMOUNT';
        }
        if ($this->isColumnModified(MovementTableMap::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'DESCRIPTION';
        }
        if ($this->isColumnModified(MovementTableMap::CREATE_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'CREATE_DATE';
        }
        if ($this->isColumnModified(MovementTableMap::UPDATE_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'UPDATE_DATE';
        }
        if ($this->isColumnModified(MovementTableMap::STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'STATUS';
        }

        $sql = sprintf(
            'INSERT INTO movement (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'ID_MOVEMENT':
                        $stmt->bindValue($identifier, $this->id_movement, PDO::PARAM_INT);
                        break;
                    case 'MOVEMENT_TYPE':
                        $stmt->bindValue($identifier, $this->movement_type, PDO::PARAM_STR);
                        break;
                    case 'ID_NATURE':
                        $stmt->bindValue($identifier, $this->id_nature, PDO::PARAM_INT);
                        break;
                    case 'PARCEL_COUNT':
                        $stmt->bindValue($identifier, $this->parcel_count, PDO::PARAM_INT);
                        break;
                    case 'CYCLE_TYPE':
                        $stmt->bindValue($identifier, $this->cycle_type, PDO::PARAM_STR);
                        break;
                    case 'CYCLE_INTERVAL':
                        $stmt->bindValue($identifier, $this->cycle_interval, PDO::PARAM_INT);
                        break;
                    case 'EXPIRATION_DAY':
                        $stmt->bindValue($identifier, $this->expiration_day ? $this->expiration_day->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'START_DATE':
                        $stmt->bindValue($identifier, $this->start_date ? $this->start_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'AMOUNT':
                        $stmt->bindValue($identifier, $this->amount, PDO::PARAM_STR);
                        break;
                    case 'DESCRIPTION':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'CREATE_DATE':
                        $stmt->bindValue($identifier, $this->create_date ? $this->create_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'UPDATE_DATE':
                        $stmt->bindValue($identifier, $this->update_date ? $this->update_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'STATUS':
                        $stmt->bindValue($identifier, (int) $this->status, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MovementTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdMovement();
                break;
            case 1:
                return $this->getMovementType();
                break;
            case 2:
                return $this->getIdNature();
                break;
            case 3:
                return $this->getParcelCount();
                break;
            case 4:
                return $this->getCycleType();
                break;
            case 5:
                return $this->getCycleInterval();
                break;
            case 6:
                return $this->getExpirationDay();
                break;
            case 7:
                return $this->getStartDate();
                break;
            case 8:
                return $this->getAmount();
                break;
            case 9:
                return $this->getDescription();
                break;
            case 10:
                return $this->getCreateDate();
                break;
            case 11:
                return $this->getUpdateDate();
                break;
            case 12:
                return $this->getStatus();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Movement'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Movement'][$this->getPrimaryKey()] = true;
        $keys = MovementTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdMovement(),
            $keys[1] => $this->getMovementType(),
            $keys[2] => $this->getIdNature(),
            $keys[3] => $this->getParcelCount(),
            $keys[4] => $this->getCycleType(),
            $keys[5] => $this->getCycleInterval(),
            $keys[6] => $this->getExpirationDay(),
            $keys[7] => $this->getStartDate(),
            $keys[8] => $this->getAmount(),
            $keys[9] => $this->getDescription(),
            $keys[10] => $this->getCreateDate(),
            $keys[11] => $this->getUpdateDate(),
            $keys[12] => $this->getStatus(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param      string $name
     * @param      mixed  $value field value
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return void
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MovementTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @param      mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdMovement($value);
                break;
            case 1:
                $this->setMovementType($value);
                break;
            case 2:
                $this->setIdNature($value);
                break;
            case 3:
                $this->setParcelCount($value);
                break;
            case 4:
                $this->setCycleType($value);
                break;
            case 5:
                $this->setCycleInterval($value);
                break;
            case 6:
                $this->setExpirationDay($value);
                break;
            case 7:
                $this->setStartDate($value);
                break;
            case 8:
                $this->setAmount($value);
                break;
            case 9:
                $this->setDescription($value);
                break;
            case 10:
                $this->setCreateDate($value);
                break;
            case 11:
                $this->setUpdateDate($value);
                break;
            case 12:
                $this->setStatus($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = MovementTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdMovement($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setMovementType($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdNature($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setParcelCount($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCycleType($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setCycleInterval($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setExpirationDay($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setStartDate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setAmount($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setDescription($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setCreateDate($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setUpdateDate($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setStatus($arr[$keys[12]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MovementTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MovementTableMap::ID_MOVEMENT)) $criteria->add(MovementTableMap::ID_MOVEMENT, $this->id_movement);
        if ($this->isColumnModified(MovementTableMap::MOVEMENT_TYPE)) $criteria->add(MovementTableMap::MOVEMENT_TYPE, $this->movement_type);
        if ($this->isColumnModified(MovementTableMap::ID_NATURE)) $criteria->add(MovementTableMap::ID_NATURE, $this->id_nature);
        if ($this->isColumnModified(MovementTableMap::PARCEL_COUNT)) $criteria->add(MovementTableMap::PARCEL_COUNT, $this->parcel_count);
        if ($this->isColumnModified(MovementTableMap::CYCLE_TYPE)) $criteria->add(MovementTableMap::CYCLE_TYPE, $this->cycle_type);
        if ($this->isColumnModified(MovementTableMap::CYCLE_INTERVAL)) $criteria->add(MovementTableMap::CYCLE_INTERVAL, $this->cycle_interval);
        if ($this->isColumnModified(MovementTableMap::EXPIRATION_DAY)) $criteria->add(MovementTableMap::EXPIRATION_DAY, $this->expiration_day);
        if ($this->isColumnModified(MovementTableMap::START_DATE)) $criteria->add(MovementTableMap::START_DATE, $this->start_date);
        if ($this->isColumnModified(MovementTableMap::AMOUNT)) $criteria->add(MovementTableMap::AMOUNT, $this->amount);
        if ($this->isColumnModified(MovementTableMap::DESCRIPTION)) $criteria->add(MovementTableMap::DESCRIPTION, $this->description);
        if ($this->isColumnModified(MovementTableMap::CREATE_DATE)) $criteria->add(MovementTableMap::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(MovementTableMap::UPDATE_DATE)) $criteria->add(MovementTableMap::UPDATE_DATE, $this->update_date);
        if ($this->isColumnModified(MovementTableMap::STATUS)) $criteria->add(MovementTableMap::STATUS, $this->status);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(MovementTableMap::DATABASE_NAME);
        $criteria->add(MovementTableMap::ID_MOVEMENT, $this->id_movement);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return   int
     */
    public function getPrimaryKey()
    {
        return $this->getIdMovement();
    }

    /**
     * Generic method to set the primary key (id_movement column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdMovement($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdMovement();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \PersonalFinance\Db\Movement (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdMovement($this->getIdMovement());
        $copyObj->setMovementType($this->getMovementType());
        $copyObj->setIdNature($this->getIdNature());
        $copyObj->setParcelCount($this->getParcelCount());
        $copyObj->setCycleType($this->getCycleType());
        $copyObj->setCycleInterval($this->getCycleInterval());
        $copyObj->setExpirationDay($this->getExpirationDay());
        $copyObj->setStartDate($this->getStartDate());
        $copyObj->setAmount($this->getAmount());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setUpdateDate($this->getUpdateDate());
        $copyObj->setStatus($this->getStatus());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return                 \PersonalFinance\Db\Movement Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_movement = null;
        $this->movement_type = null;
        $this->id_nature = null;
        $this->parcel_count = null;
        $this->cycle_type = null;
        $this->cycle_interval = null;
        $this->expiration_day = null;
        $this->start_date = null;
        $this->amount = null;
        $this->description = null;
        $this->create_date = null;
        $this->update_date = null;
        $this->status = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MovementTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

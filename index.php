<?php
error_reporting(E_ALL);

require_once __DIR__.'/vendor/autoload.php'; 

use Propel\Runtime\Propel;
use Propel\Runtime\Connection\ConnectionManagerSingle;

$serviceContainer = Propel::getServiceContainer();
$serviceContainer->setAdapterClass('default', 'mysql');
$manager = new ConnectionManagerSingle();
$manager->setConfiguration(array (
  'dsn'      => 'mysql:host=localhost;dbname=personalfinance',
  'user'     => 'root',
  'password' => '09azaz',
));
$serviceContainer->setConnectionManager('default', $manager);

$app = new Silex\Application(); 

$app->get('/', "PersonalFinance\Controller\IndexController::indexAction");

$app->get('/{name}', function($name) use($app) { 
    return 'Hello '.$app->escape($name); 
}); 

$app->run(); 
